# Jenkins Pipeline for AWS ECS Deployment with Docker and Terraform

### Overall Goal

The goal of this procedure is to be able to deploy a new image of your application (In this case tomcat8) to Amazon Elastic Container Service(ECS).
This image will be built by a Jenkins Pipeline script, which will automatically detect when a change in your git repo has occurred, and deploy it to ECS.
The Elastic Container Registry(ECR) will serve as the repository for your application image.
Terraform will build the ECR and ECS services, as well as all of the supporting networks and IAM policies.
The Jenkins server containing the Pipeline will be created by docker-compose on your local machine.

---

### Prerequisities
* Install docker for your platform.  My work was done entirely on an MacBook Pro (Mojave)
* Install docker-compose. I used docker-compose version 1.14.0.
* Install terraform.  I used Terraform v0.11.14 for my install.
* Install git
* An Amazon Web Services(AWS) account with API credentials
* A git repository and username with access
* A sense of fun and adventure!

---

### Initial Setup
1. Download all files from this git repository
2. Inside the **jenkins** folder, edit the **docker-compose.yml** file so that:
     * On line **12** replace "/Users/eric.musican/git-work/test/jenkins" with the full path of your jenkins directory.
3. Run the following commands to export your AWS credentials:
```
export AWS_ACCESS_KEY_ID=<Insert your AWS_ACCESS_KEY_ID>
export AWS_SECRET_ACCESS_KEY=<Insert your AWS_SECRET_ACCESS_KEY>
```
4. Inside the **terraform-ecs-ecr** folder, edit the **variables.tf** file so that all values in "<>" are filled with your applications' values.
5. Inside the **tomcat8-jenkins-pipeline** folder, edit the **Jenkinsfile** file so that all values in "<>" are filled with your applications' values. These can be found near the top of the file after the **<environment>** section.
6. Push the **tomcat8-jenkins-pipeline** folder to your git repo
7. Prepare the jenkins directory by adding a **.aws/credentials** file with the AWS values for the user that Jenkins will use to deploy ECS

---

### Building With Terraform
1. Change to the **terraform-ecs-ecr** directory.
2. Run the following commands in order:
```
terraform init
terraform plan
terraform apply
```
3. After it completes successfully, Note the **alb_hostname** output url for later.

---

### Build and Configure Jenkins
1. Change to the **jenkins** directory.
2. Run the following command:
```
docker-compose up -d
```
3. In a web browser navigate to [http://localhost:8080](http://localhost:8080)
4. Two containers will spin up.  The first is a socat container for docker.sock redirection, and the second is the Jenkins server.
5. Run the 'docker logs' command on the Jenkins container to grab the administrative password, and login.
6. Select "Install suggested plugins"
7. Create the admin user.
8. Select "Manage Jenkins" -> "Plugin Manager" then click on the "Available" tab.
9. Install the following Jenkins Plugins:
    * amazon-ecr
    * cloudbees-aws-credentials
    * docker
10. Go back to the Jenkins home screen and select "Credentials" -> System, then "Global Credentials" -> "Add Credentials"
11. Add an normal user, or one with ssh keys. This user will used to push and pull from the git repo where **tomcat8-jenkins-pipeline** is located.
12. Add an AWS user. When you edited the **Jenkinsfile** earlier, the **ECR_USER** field was populated.  This same username must be a Jenkins user, and the AWS keys must match.
13. On the main Jenkins page, create a new job.  Select Pipeline for type.
14. In the job configuration at the **Pipeline** section, select **Pipeline script from SCM**.  Select **git** as the SCM.
15. In the **repositories** section, put in the git repo where **tomcat8-jenkins-pipeline** is stored. Select the git user for **Credentials**.
16. **Run the pipeline for the first time**

---

### Check that the application is functioning

Navigate to [http://<your_alb_output_url>:8080](http://<your_alb_output_url>:8080)
